<?php
include 'config.php';
// checks the $imgs array and gets the background image to use
if (count($imgs) > 1) {
	if (!isset($_GET["ubar"])) {
		die("something went horribly wrong.");
	}
	$orim = dic($imgs[$_GET["ubar"]]);	
}
elseif (count($imgs) == 0) {
	die('error');
}
else {
	$orim = dic($imgs[0]);
}
if (!isset($_GET["uid"]) || !isset($_GET["mode"])) {
	die("something went horribly wrong.");
}
// Get contents from API
$oafgc = file_get_contents("http://osu.ppy.sh/api/get_user?k=" . $apikey . "&u=" . urlencode($_GET["uid"]) . "&type=string&m=" . $_GET["mode"]);
$oafgcd = json_decode($oafgc);
// Checks the mode
switch ($_GET["mode"]) {
	case 0:
	$gmtu = 'osu!';
	break;
	case 1:
	$gmtu = 'Taiko';
	break;
	case 2:
	$gmtu = 'CtB';
	break;
	default:
	$gmtu = 'Something went wrong'; break;
}
// Puts the texts to the image
$font = 'visitor2.ttf';
$textin = array(
	"score" 	=> 		mb_substr(strval($oafgcd[0]->total_score / 1000000000), 0, 5) . 'B',
	"acc"		=>		mb_substr(strval($oafgcd[0]->accuracy), 0, 5) . '%',
	"lvl"		=>		intval($oafgcd[0]->level, 0),
	"rank"		=>		number_format($oafgcd[0]->pp_rank)
	);
$fontcolor = imagecolorallocate($orim, 255, 255, 255);
imagettftext($orim, 12, 0, 5, 13, $fontcolor, $font, $gmtu);
if ($gmtu != 'Something went wrong') {
imagettftext($orim, 12, 0, 40, 13, $fontcolor, $font, 'score: ' . $textin["score"]);
imagettftext($orim, 12, 0, 130, 13, $fontcolor, $font, 'acc: ' . $textin["acc"]);
imagettftext($orim, 12, 0, 204, 13, $fontcolor, $font, 'lvl: ' . $textin["lvl"]);
imagettftext($orim, 12, 0, 255, 13, $fontcolor, $font, "rank: #" . $textin["rank"]);
}
// output.
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
header('Content-type: image/png');
imagepng($orim);
imagedestroy($orim)
?>